from kivy.app import App
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.label import Label
from kivy.properties import BooleanProperty
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.network.urlrequest import UrlRequest
import json

print("KIVY Loading 1")

Builder.load_string('''
<SelectableLabel>:
    # Draw a background to indicate selection
    canvas.before:
        Color:
            rgba: (.0, 0.9, .1, .3) if self.selected else (0, 0, 0, 1)
        Rectangle:
            pos: self.pos
            size: self.size
<RV>:
    viewclass: 'SelectableLabel'
    SelectableRecycleBoxLayout:
        default_size: None, dp(56)
        default_size_hint: 1, None
        size_hint_y: None
        height: self.minimum_height
        orientation: 'vertical'
        multiselect: False
        touch_multiselect: False

<PXL>:
    orientation: 'horizontal'

<BL>:
    orientation: 'vertical'
    RV:
        size_hint_y: 0.8
    PXL:
        size_hint_y: 0.1
    Upload:
        text: 'Uploader'
        size_hint_y: 0.1
''')

class SelectableRecycleBoxLayout(FocusBehavior, LayoutSelectionBehavior,
                                 RecycleBoxLayout):
    ''' Adds selection and focus behaviour to the view. '''

class SelectableLabel(RecycleDataViewBehavior, Label):
    ''' Add selection support to the Label '''
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        return super(SelectableLabel, self).refresh_view_attrs(
            rv, index, data)

    def on_touch_down(self, touch):
        ''' Add selection on touch down '''
        if super(SelectableLabel, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected
        if is_selected:
            name = rv.data[index]['text']
            headers = {'Content-type': 'application/json'}
            UrlRequest(f"{BACKEND_URL}/colours-api", method="POST", req_body=json.dumps({"name": name}), req_headers=headers)
            print("selection changed to {0}".format(rv.data[index]))

    def delete_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected
        if is_selected:
            name = rv.data[index]['text']
            headers = {'Content-type': 'application/json'}
            UrlRequest(f"{BACKEND_URL}/colours-api", method="DELETE", req_body=json.dumps({"name": name}), req_headers=headers)
            print("selection changed to {0}".format(rv.data[index]))

class RV(RecycleView):
    def __init__(self, **kwargs):
        super(RV, self).__init__(**kwargs)
        r = UrlRequest(f"{BACKEND_URL}/colours-api", method="GET")
        r.wait()
        print(f"KIVY FOUND {r.result}")
        self.data = [{'text': x} for x in r.result['animations']]


class PXL(BoxLayout):
    def __init__(self, **kwargs):
        super(PXL, self).__init__(**kwargs)
        r = UrlRequest(f"{BACKEND_URL}/num-pixels-api", method="GET")
        r.wait()
        print(f"KIVY FOUND {r.result}")
        for n in r.result['num_pixels']:
            button = Button(text=str(n), on_press=callback_num_pixels)
            button.my_value = n
            self.add_widget(button)


def callback_num_pixels(instance):
    headers = {'Content-type': 'application/json'}
    UrlRequest(f"{BACKEND_URL}/num-pixels-api",
               method="POST",
               req_body=json.dumps({"num_pixels": instance.my_value}),
               req_headers=headers)


class BL(BoxLayout):
    def __init__(self, **kwargs):
        print("Kivy BUILDING BOXLAYOUT")
        super(BL, self).__init__(**kwargs)


class Upload(Button):
    def __init__(self, **kwargs):
        super(Upload, self).__init__(**kwargs)


class LEDApp(App):
    def build(self):
        print("Kivy BUILDING LAYOUT")
        return BL()


#if __name__ == '__main__':
print("Finding server")
    #for n in range(151, 200):
BACKEND_URL = f'http://192.168.62.151:5000'
    #    r = requests.get(f"{BACKEND_URL}/colours")
    #    try:
    #        r.json()['animations']
    #        break
    #    except (AttributeError, KeyError):
    #        pass
    #else:
    #    print("No server found")
    #    sys.exit(1)
print("Kivy STARTING APP")
LEDApp().run()
