import sys


class animation:
    # Plain blue pixels
    BLUE = (0, 0, 255)
    ANIMATION = [BLUE]
    fast = False

    def set_length(self, n):
        self.ANIMATION = [self.BLUE]*n

    def __getitem__(self, i):
        return self.ANIMATION


# Convert the animation class to be the module itself
# See https://stackoverflow.com/questions/2447353/getattr-on-a-module
sys.modules[__name__] = animation()
