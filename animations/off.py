import sys


class animation:
    # Plain no-colour pixels
    OFF = (0, 0, 0)
    ANIMATION = [OFF]

    def set_length(self, n):
        self.ANIMATION = [self.OFF]*n

    def __getitem__(self, i):
        return self.ANIMATION


# Convert the animation class to be the module itself
# See https://stackoverflow.com/questions/2447353/getattr-on-a-module
sys.modules[__name__] = animation()

