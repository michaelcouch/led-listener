import React, {useState} from 'react';
import { StatusBar } from 'expo-status-bar';
import { TouchableOpacity, StyleSheet, Text, View, Button } from 'react-native';


class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initialize: false,
      options: []
    };

    this.handlePress = this.handlePress.bind(this);
  }

  componentDidMount() {
    this.handlePress()
  }

  handlePress () {
    {
      fetch('http://192.168.1.98:5000/colours-api')
       .then(response => response.json())
       .then(json => {console.log(json);
                      this.setState({initialized: true, options: json.animations})}
       )
       .catch(this.setState({options: [{data: 'Error', selected: false}], initialized: false}));
    }
  }

  handle(name) {
    var requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({'name': name})
    };
    fetch('http://192.168.1.98:5000/colours-api', requestOptions)
      .then(response => this.handleSelectionUpdate(name))
  }

  handleSelectionUpdate(newName) {
    this.setState({
      options: this.state.options.map(item => ({name: item.name, selected: (item.name == newName)}))
    })
  }

  render() {
    var data = this.state.options;
    var initialized = this.state.initialized;
    if (initialized) {
      return (
        <View style={styles.container}>
          <View style={styles.column}>
            {data.filter((_,i) => (i < 8) && (i % 2 == 0)).map(item =>
              <AnimationBox key={item.name} name={item.name} selected={item.selected} handler={(name) => this.handle(name)} />
            )}
          </View>
          <View style={{flexDirection: 'column', alignItems: 'center'}}>
            {data.filter((_,i) => (i < 8) && (i % 2 == 1)).map(item =>
              <AnimationBox key={item.name} name={item.name} selected={item.selected} handler={(name) => this.handle(name)} />
            )}
          </View>
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
          <Text style={styles.baseText}>
            CONNECTING TO MAINFRAME...
          </Text>
        <Button onPress={this.handlePress} title="BUTTON">BUTTON</Button>
        </View>
      )
    }
  }
}

class AnimationBox extends React.Component {
  constructor (props) {
    super(props);
  };


  render () {
    return (
      <View style={[styles.aniBox, this.props.selected ? styles.selected : {}]}>
        <TouchableOpacity onPress={this.props.handler.bind(this, this.props.name)}
                style={[styles.aniButton]}
                title={this.props.name}><Text style={styles.baseText}>{this.props.name}</Text></TouchableOpacity>
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 43,
    paddingVertical: '10%',
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#111111',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  column: {
    flexDirection: 'column',
    marginHorizontal: 5,
    flex: 1,
  },
  aniButton: {
    flex: 1,
  },
  selected: {
    backgroundColor: '#002200'
  },
  aniBox: {
    //margin: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    width: 150,
    height: 150,
    marginVertical: 10,
    borderWidth: 1,
    borderColor: '#00FF00',
    backgroundColor: '#000000'
  },
  baseText: {
    color: '#00FF00',
    fontFamily: 'monospace'
  }
});


export default App
