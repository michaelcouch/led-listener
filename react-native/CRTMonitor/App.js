import React, {useState} from 'react';
import { StatusBar } from 'expo-status-bar';
import { TouchableOpacity, TextInput, StyleSheet, Text, View, Button } from 'react-native';
import * as Font from 'expo-font';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initialize: false,
      options: [],
      url: '192.168.1.98'
    };

    this.handleUrl = this.handleUrl.bind(this);
  }

  _loadFontsAsync = async () => {
    //loadAsync returns true | error
    let isLoaded = await Font.loadAsync({
      // add as many fonts as you want here .... 
      TerminalFont: require("./assets/fonts/VT323-Regular.ttf")
    });
  };
  componentDidMount() {
    this._loadFontsAsync();
    this.handleUrl();
  }

  handleUrl (text) {
    var url = text ? text : this.state.url;
    this.setState({initialized: false});
    fetch('http://'+url+':5000/colours-api')
     .then(response => response.json())
     .then(json => this.setState({url: url, initialized: true, options: json.animations}))
     .catch(this.setState({loptions: [{data: 'Error', selected: false}], initialized: false}));
  }

  render() {
    var data = this.state.options;
    var initialized = this.state.initialized;
    if (initialized) {
      return (
        <View style={styles.container}>
          <Header url={this.state.url} reconnector={(text) => this.handleUrl(text)}/>
          <AnimationMenu data={data} url={this.state.url}/>
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
          <Header url={this.state.url} reconnector={(text) => this.handleUrl(text)}/>
          <View style={styles.connecting}>
            <Text style={styles.baseText}>
              CONNECTING TO MAINFRAME...
            </Text>
          </View>
        </View>
      )
    }
  }
}

class Header extends React.Component {
  constructor (props) {
    super(props);
    this.state = {text: props.url}
  };

  onChangeText = (newText) => {
    this.setState({text: newText});
  };

  render() {
    return (
      <View style={styles.header}>
        <TextInput style={styles.baseText} value={this.state.text} onChangeText={this.onChangeText}/>
        <TouchableOpacity onPress={this.props.reconnector.bind(this, this.state.text)}
                          style={[styles.connectButton]}>
          <Text style={styles.baseText}>RECONNECT</Text></TouchableOpacity>
      </View>
    )
  }
}

class AnimationMenu extends React.Component {
  constructor (props) {
    super(props);
    this.state = {data: props.data, url: props.url}
  };

  handle(name) {
    var requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({'name': name})
    };
    fetch('http://'+this.state.url+':5000/colours-api', requestOptions)
      .then(response => this.handleSelectionUpdate(name))
  }

  handleSelectionUpdate(newName) {
    this.setState({
      data: this.state.data.map(item => ({name: item.name, selected: (item.name == newName)}))
    })
  }

  render() {
    var data = this.state.data;
    const N = 18;
    const cols = 3;
    return (
        <View style={styles.menu}>
          <View style={styles.column}>
            {data.filter((_,i) => (i < N) && (i % cols == 0)).map(item =>
              <AnimationBox key={item.name} name={item.name} selected={item.selected} handler={(name) => this.handle(name)} />
            )}
          </View>
          <View style={styles.column}>
            {data.filter((_,i) => (i < N) && (i % cols == 1)).map(item =>
              <AnimationBox key={item.name} name={item.name} selected={item.selected} handler={(name) => this.handle(name)} />
            )}
          </View>
          <View style={styles.column}>
            {data.filter((_,i) => (i < N) && (i % cols == 2)).map(item =>
              <AnimationBox key={item.name} name={item.name} selected={item.selected} handler={(name) => this.handle(name)} />
            )}
          </View>
        </View>
    )
  }
}

class AnimationBox extends React.Component {
  constructor (props) {
    super(props);
  };

  render () {
    return (
      <View style={[styles.aniBox, this.props.selected ? styles.selected : {}]}>
        <TouchableOpacity onPress={this.props.handler.bind(this, this.props.name)}
                style={[styles.aniButton]}
                title={this.props.name}><Text style={styles.baseText}>{this.props.name}</Text></TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: '20%',
    paddingBottom: '10%',
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#111111',
  },
  header: {
    flexDirection: 'row',
    paddingHorizontal: '10%',
    justifyContent: 'space-between'
  },
  connecting: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
  },
  menu: {
    paddingHorizontal: 43,
    paddingVertical: '5%',
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  column: {
    flexDirection: 'column',
    marginHorizontal: 5,
    flex: 1,
  },
  connectButton: {
    borderWidth: 1,
    borderColor: '#00FF00',
    backgroundColor: '#000000',
    padding: 5
  },
  aniButton: {
    flex: 1,
    padding: 3,
  },
  selected: {
    backgroundColor: '#003F00'
  },
  aniBox: {
    flexDirection: 'column',
    justifyContent: 'center',
    width: 90,
    height: 90,
    marginVertical: 10,
    borderWidth: 1,
    borderColor: '#00FF00',
    backgroundColor: '#000000'
  },
  baseText: {
    color: '#00FF00',
    fontFamily: 'TerminalFont',
    fontSize: 8,
  }
});

export default App
