from flask import Flask, request, jsonify, flash, url_for, redirect
from flask_cors import CORS
import os
from shutil import move as mv
import json
from werkzeug.utils import secure_filename
from image_convert import converter
import time
"""A Web server to select the animation to use the pixel server"""

UPLOAD_FOLDER = 'images'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app = Flask(__name__)
CORS(app)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def update_config(animation_path=None, num_pixels=None, brightness=None,
                  framelength=None):
    config = read_config()
    if animation_path is not None:
        config["animation_path"] = animation_path
    if num_pixels is not None:
        config["num_pixels"] = num_pixels
    if brightness is not None:
        config["brightness"] = brightness
    if framelength is not None:
        config["framelength"] = framelength
    with open('config_to_use.json', 'w') as f:
        json.dump(config, f)


def read_config():
    with open('config_to_use.json', 'r') as f:
        config = json.load(f)
    return config


@app.route("/colours-api", methods=["GET", "POST", "DELETE"])
def coloursapi():
    config = read_config()
    animations = [
                  {'name': a[:-3],
                   'selected': a[:-3] == config['animation_path']}
                  for a in os.listdir("animations") if '.py' in a]
    if request.method == "GET":
        time.sleep(1)
        resp = jsonify({"animations": animations})
        return resp
    if request.method == "POST":
        name = request.get_json().get("name")
        print(name)
        if name in [a['name'] for a in animations]:
            update_config(animation_path=name)
            resp = jsonify({'name': name})
            resp.status_code = 200
        else:
            resp = jsonify(success=False)
            resp.status_code = 400
        return resp
    if request.method == "DELETE":
        name = request.get_json().get("name")
        if name in animations:
            t = str(round(time.time()))
            pydata = f"{name}.data"
            if pydata in os.listdir("animations"):
                mv(f'animations/{name}.py', f'old-animations/{name}-{t}.py')
                mv(f'animations/{pydata}', f'old-animations/{name}-{t}.data')
            else:
                resp = jsonify(success=False)
                resp.status_code = 400
                return resp
            with open('config_to_use.json', 'r') as f:
                switch = json.load(f)["animation_path"] == name
            if switch:
                update_config(animation_path='blue', brightness=0.1)
            resp = jsonify(success=True)
            resp.status_code = 200
        else:
            resp = jsonify(success=False)
            resp.status_code = 400
        return resp


@app.route("/num-pixels-api", methods=["GET", "POST"])
def num_pixels_api():
    valid = [245, 98]
    if request.method == "GET":
        resp = jsonify({"num_pixels": valid})
        return resp
    if request.method == "POST":
        num_pixels = request.get_json().get("num_pixels")
        if num_pixels in valid:
            update_config(num_pixels=num_pixels)
            resp = jsonify(success=True)
            resp.status_code = 200
            return resp
    resp = jsonify(success=False)
    resp.status_code = 400
    return resp


@app.route("/brightness-api", methods=["GET", "POST"])
def brightness_api():
    valid = [0, 1]
    if request.method == "GET":
        resp = jsonify({"brightness": valid})
        return resp
    if request.method == "POST":
        brightness = request.get_json().get("brightness")
        if brightness >= valid[0] and brightness <= valid[1]:
            update_config(brightness=brightness)
            resp = jsonify(success=True)
            resp.status_code = 200
            return resp
    resp = jsonify(success=False)
    resp.status_code = 400
    return resp


@app.route("/framelength-api", methods=["GET", "POST"])
def framelength_api():
    if request.method == "GET":
        resp = jsonify({"framelength": 'float'})
        return resp
    if request.method == "POST":
        framelength = request.get_json().get("framelength")
        try:
            fr = float(framelength)
            update_config(framelength=fr)
            resp = jsonify(success=True)
            resp.status_code = 200
            return resp
        except ValueError:
            pass
    resp = jsonify(success=False)
    resp.status_code = 400
    return resp


@app.route("/colours/", defaults={"name": ''})
@app.route("/colours/<name>")
def colours(name):
    page = ''
    animations = [a[:-3] for a in os.listdir("animations") if '.py' in a]

    if name in animations:
        update_config(animation_path=name)
        page += f"<h2>Starting {name}</h2>" if name else ''
    else:
        page += "<h2>Animation {name} not found</h2>"

    for animation in animations:
        page += f"""<br><a href="/colours/{animation}">{animation}</a>"""
    page += """<br><a href="/upload">Upload another animation</a>"""
    return page


@app.route('/upload-api', methods=['POST'])
def upload_file_api():
    if request.method == "POST":
        # check if the post request has the file part
        if 'file' not in request.files:
            resp = jsonify(success=False)
            resp.status_code = 400
            return resp
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            resp = jsonify(success=False)
            resp.status_code = 400
            return resp
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            location = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(location)
            print(f"Saved {file.filename} as {filename}")
            print("Converting...")
            converter(location)
            print("Successfully converted")
            resp = jsonify(success=True)
            resp.status_code = 200
            return resp
            name = filename.split('.')[0].split('/')[-1]
            return redirect(url_for('colours_api', name=name))


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            location = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(location)
            print(f"Saved {file.filename} as {filename}")
            print("Converting...")
            converter(location)
            name = filename.split('.')[0].split('/')[-1]
            return redirect(url_for('colours', name=name))
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''
