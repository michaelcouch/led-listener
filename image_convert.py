from PIL import Image
import numpy as np


def converter(path):
    image = Image.open(f'{path}')
    data = np.asarray(image)
    # 3 channels only please
    r, g, b = data.transpose(2, 1, 0)[:3]
    # order is GRB for pixels
    data = np.array([g,r,b], dtype=np.uint8)
    data = data.transpose(2, 1, 0)
    #data = data.transpose(2, 1, 0)[:3].transpose(2, 1, 0)
    pixel_width = data.shape[1]
    data_length = data.shape[0]
    outpath = path.split('/')[-1].split('.')[0]
    with open(f'animations/{outpath}.data', 'wb') as f:
        f.write(data.tobytes())

    # Prepare a class that implements this image as an animation
    module =     """import sys"""
    module +=  """\nimport numpy as np"""
    module +=  """\n"""
    module +=  """\ndef load_animation():"""
    module += f"""\n    with open('animations/{outpath}.data','rb') as f:"""
    module +=  """\n        data = np.frombuffer(f.read(), dtype='uint8')"""
    module += f"""\n    data = data.reshape({data_length},{pixel_width},3)"""
    module +=  """\n    return data"""
    module +=  """\n"""
    module +=  """\nclass animation:"""
    module +=  """\n    # Image based pixel animations"""
    module +=  """\n    ANIMATION = load_animation()"""
    module +=  """\n    fast = True"""
    module +=  """\n"""
    module +=  """\n    def __getitem__(self, tick):"""
    module += f"""\n        return self.ANIMATION[tick % {data_length}]"""
    module +=  """\n"""
    module +=  """\nsys.modules[__name__] = animation()"""

    with open(f'animations/{outpath}.py', 'w') as f:
        f.write(module)

if __name__ == "__main__":
    import sys
    path = sys.argv[1]
    converter(path)
