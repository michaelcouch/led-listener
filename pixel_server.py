import time
import board
import neopixel
import json
from importlib import import_module


def get_animation(config):
    # Get the animation from the animations directory
    animation_path = config['animation_path']
    try:
        animation = import_module(f'animations.{animation_path}')
        animation.set_length(config['num_pixels'])
    except ModuleNotFoundError as e:
        print(f"Animation {animation_path} not found: {e}")
        print("Defaulting to plain red animation")
        animation = [[(255, 0, 0) for i in range(config['num_pixels'])]]
    except AttributeError:
        print("Warning: Animation has no set_length method")
    return animation


def initialize_board(num_pixels, brightness):
    # Start the pixel animator
    pixel_pin = board.D18
    order = neopixel.GRB
    # Don't go too bright - must keep current low so we don't
    # burn out the powerbank
    clipped_brightness = max(brightness, 30 / num_pixels)
    # Initialize the pixels
    pixels = neopixel.NeoPixel(
        pixel_pin, num_pixels, brightness=clipped_brightness,
        auto_write=False, pixel_order=order
    )
    return pixels


def get_config():
    # read the pixel config from a file. This gets updated by the web server.
    with open("config_to_use.json") as f:
        config = json.load(f)
    return config


def cycle(tick, animation):
    # Actually animate
    # Get the pixels to animate
    frame = animation[tick]
    # set the pixel colours
    m = min(len(pixels), len(frame))
    if animation.fast:
        pixels._post_brightness_buffer[: 3*m] = (
            frame[: 3*m] // (int(1 / pixels._brightness))
        ).tobytes()
    else:
        pixels[:m] = frame[:m]

    pixels.show()



config = get_config()
animation = get_animation(config)
num_pixels = config['num_pixels']
brightness = config['brightness']
framelength = config['framelength']
pixels = initialize_board(num_pixels, brightness)

check_start = time.time()
if __name__ == "__main__":
    tick = 0
    while True:
        start = time.time()
        # update colours
        cycle(tick, animation)
        tick += 1

        # check to see if the config changed every 2 seconds
        if time.time() - check_start  > 2:
            check_start = time.time()
            new_config = get_config()
            if new_config != config:
                # Config changed
                print(f"Loading new config {new_config}")
                config = new_config
                if (config['num_pixels'] != num_pixels
                   or config['brightness'] != brightness):
                    # Basic parameters of the pixels changed. Update
                    num_pixels = config['num_pixels']
                    brightness = config['brightness']
                    pixels = initialize_board(num_pixels, brightness)
                framelength = config['framelength']
                animation = get_animation(config)
                try:
                    animation.fast
                except AttributeError:
                    animation.fast = False
                tick = 0
        # wait before the next animation
        time.sleep(max(0, framelength - (time.time() - start)))

