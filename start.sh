#!/bin/bash

HERE="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [ "$1" == "http" ]
then
  export FLASK_APP="$HERE"/webapp
  python -m flask run --host=0.0.0.0
elif [ "$1" == "led" ]
then
  python3 $HERE/pixel_server.py
else
  echo "Invalid argument, choose led or http"
  exit 1
fi
