#!/usr/bin/env bash

HOST=192.168.1.97

rsync --info=progress2 -r animations mcouch@$HOST:/srv/led-listener
rsync --info=progress2 -r *.py mcouch@$HOST:/srv/led-listener
rsync --info=progress2 -r start.sh mcouch@$HOST:/srv/led-listener
rsync --info=progress2 -r images mcouch@$HOST:/srv/led-listener
rsync --info=progress2 -r old-animations mcouch@$HOST:/srv/led-listener
ssh -t mcouch@$HOST "sudo systemctl daemon-reload && sudo systemctl restart led && sudo systemctl restart listener"
